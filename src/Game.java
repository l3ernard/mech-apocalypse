import javax.imageio.ImageIO;
import javax.swing.JPanel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.File;
import java.util.Arrays;

/**
 * Created by Bernard on 29-Oct-14.
 */
public class Game extends JPanel implements KeyListener {

    private BufferedImage playerSprite;
    private BufferedImage mapHPEGSprite;
    private BufferedImage terrainSprite;
    private BufferedImage enemySprite;

    public static int playerWidth = 32;
    public static int playerHeight = 32;
    final int playerRows = 8;
    final int playerCols = 4;

    final int mapHPEGWidth = 128;
    final int mapHPEGHeight = 160;
    final int mapHPEGRows = 3;
    final int mapHPEGCols = 1;

    final int terrainSpriteWidth = 32;
    final int terrainSpriteHeight = 32;
    final int terrainSpriteRows = 1;
    final int terrainSpriteCols = 1;

    final int enemySpriteWidth = 32;
    final int enemySpriteHeight = 32;
    final int enemySpriteRows = 1;
    final int enemySpriteCols = 1;
    public static int enemyWidth = 32;
    public static int enemyHeight = 32;

    public static int curPlayerSprite = 0;
    public static int curMapHPEGSprite = 0;

    BufferedImage[] playerSprites = new BufferedImage[playerRows * playerCols];
    BufferedImage[] mapHPEGSprites = new BufferedImage[mapHPEGRows * mapHPEGCols];
    BufferedImage[] terrainSprites = new BufferedImage[terrainSpriteRows * terrainSpriteCols];
    BufferedImage[] enemySprites = new BufferedImage[enemySpriteRows * enemySpriteCols];


    //player movement shit
    public static int pLocationY = 50*32;
    public static int pLocationX = 50*32;
    public static int pDirection = 0; // -1 left; 1 right; 2 up; -2 down; 3 upleft; 4 upright; -4 downleft; -3 downright
    public static int pVelocity = 1;

    public static int eLocationX = pLocationX + 50;
    public static int eLocationY = pLocationY + 50;

    //playerstats
    public static int pEnergy = 10000;
    public static int pHealth = 10000;
    public static int pAmmo = 10000;


    //g.translate shit cam follow player
    public static int camX = (pLocationX  - Main.gWidth/2) + playerWidth / 2;
    public static int camY = (pLocationY  - Main.gHeight/2)  + playerHeight / 2;

    //laser shizzlekabizzle
    public static int startX = pLocationX + playerWidth / 2;
    public static int startY = pLocationY + playerHeight / 2;



    public static int endX = 0;
    public static int endY = 0;

    //check keys enzo
    public static boolean keyLeft = false;
    public static boolean keyRight = false;
    public static boolean keyUp = false;
    public static boolean keyDown = false;
    public static boolean keySpace = false;

    public static boolean pMoving = false;
    public static boolean pShooting = false;

    public Game(Manager managerclass){
        try {
            playerSprite = ImageIO.read(new File("res/player_spritesheet.png"));
            mapHPEGSprite = ImageIO.read(new File("res/hud_spritesheet.png"));
            terrainSprite = ImageIO.read(new File("res/terrain_spritesheet.png"));
            enemySprite = ImageIO.read(new File("res/enemy_spritesheet.png"));

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        for (int i = 0; i < playerRows; i++)
        {
            for (int j = 0; j < playerCols; j++)
            {
                playerSprites[(i * playerCols) + j] = playerSprite.getSubimage(
                        j * playerWidth,
                        i * playerHeight,
                        playerWidth,
                        playerHeight
                );
            }
        }

        for (int i = 0; i < mapHPEGRows; i++)
        {
            for (int j = 0; j < mapHPEGCols; j++)
            {
                mapHPEGSprites[(i * mapHPEGCols) + j] = mapHPEGSprite.getSubimage(
                        j * mapHPEGWidth,
                        i * mapHPEGHeight,
                        mapHPEGWidth,
                        mapHPEGHeight
                );
            }
        }

        for (int i = 0; i < terrainSpriteRows; i++)
        {
            for (int j = 0; j < terrainSpriteCols; j++)
            {
                terrainSprites[(i * terrainSpriteCols) + j] = terrainSprite.getSubimage(
                        j * terrainSpriteWidth,
                        i * terrainSpriteHeight,
                        terrainSpriteWidth,
                        terrainSpriteHeight
                );
            }
        }


        for (int i = 0; i < enemySpriteRows; i++)
        {
            for (int j = 0; j < enemySpriteCols; j++)
            {
                enemySprites[(i * enemySpriteCols) + j] = enemySprite.getSubimage(
                        j * enemySpriteWidth,
                        i * enemySpriteHeight,
                        enemySpriteWidth,
                        enemySpriteHeight
                );
            }
        }

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.translate(-camX, -camY);
        for(int x = 0; x < 100; x++){
           for(int y = 0; y < 100; y++){
               if(y*32 > (pLocationY - 316) && y*32 < (pLocationY + 316)){
                   if(x*32 > (pLocationX - 416) && x*32 < (pLocationX + 416)){
                       g.drawImage(terrainSprites[0], x*32, y*32, null);
                   }
               }
           }
        }

        g.drawImage(playerSprites[curPlayerSprite], pLocationX, pLocationY, null);
        g.drawImage(mapHPEGSprites[curMapHPEGSprite], camX + 5, camY + 5, null);
        g.drawImage(enemySprites[0], eLocationX, eLocationY, null);


        Color  c = new Color(255, 255, 0); // Energy Bar
        Color  d = new Color(0, 0, 0); // Energy Bar
        g.setColor(c); // Energy Bar
        g.fillRect(camX + 25, camY + 134, pEnergy / 100, 10); // Energy Bar
        g.setColor(d); // Energy Bar
        g.drawRect(camX + 25, camY + 134, 100, 10); // Energy Bar

        Color  e = new Color(255, 0, 0); // Energy Bar
        Color  f = new Color(0, 0, 0); // Energy Bar
        g.setColor(e); // Energy Bar
        g.fillRect(camX + 25, camY + 149, pHealth / 100, 10); // Energy Bar
        g.setColor(f); // Energy Bar
        g.drawRect(camX + 25, camY + 149, 100, 10); // Energy Bar

        if(pShooting){
            g.drawLine(startX, startY, endX, endY);
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if(Manager.gRunning == true){

            //ACTIVATE KEYS


            if (key == KeyEvent.VK_SPACE) {
                keySpace = true;
            }

            if (key == KeyEvent.VK_LEFT) {
                keyLeft = true;
            }

            if (key == KeyEvent.VK_RIGHT) {
                keyRight = true;
            }

            if (key == KeyEvent.VK_UP) {
                keyUp = true;
            }

            if (key == KeyEvent.VK_DOWN) {
                keyDown = true;
            }


            //CHECK KEYS

            if(keyLeft){
                pMoving = true;
                pDirection = 0;
            }

            if(keyRight){
                pMoving = true;
                pDirection = 1;
            }

            if(keyUp){
                pMoving = true;
                pDirection = 2;
            }

            if(keyDown){
                pMoving = true;
                pDirection = 3;
            }

            if(keyUp && keyLeft){
                pMoving = true;
                pDirection = 4;
            }

            if(keyUp && keyRight){
                pMoving = true;
                pDirection = 5;
            }

            if(keyDown && keyLeft){
                pMoving = true;
                pDirection = 6;
            }

            if(keyDown && keyRight){
                pMoving = true;
                pDirection = 7;
            }

            if(keySpace){
                pShooting = true;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        //DEACTIVATE KEYS
        if(Manager.gRunning == true){
            if (key == KeyEvent.VK_LEFT) {
                keyLeft = false;
                pMoving = false;
            }

            if (key == KeyEvent.VK_RIGHT) {
                keyRight = false;
                pMoving = false;
            }

            if (key == KeyEvent.VK_UP) {
                keyUp = false;
                pMoving = false;
            }

            if (key == KeyEvent.VK_DOWN) {
                keyDown = false;
                pMoving = false;
            }

            if (key == KeyEvent.VK_SPACE) {
                keySpace = false;
                pShooting = false;
            }

        }
    }

    public void Update(){
        if(Manager.gRunning == true){

            //player movement
            if(pEnergy <= 0){
                pMoving = false;
            }

            if(pAmmo <= 0){
                pShooting = false;
            }
            //hitdetection
            hit:
            {
                for (int x = eLocationX; x <= (eLocationX + 32); x++) {
                    for (int y = eLocationY; y <= (eLocationY + 32); y++) {
                        if(pLocationX + 32  >= x && pLocationY + 32  >= y){
                            if(pLocationX  <= x && pLocationY  <= y) {
                                pVelocity = 0;
                                break hit;
                            } else {
                                pVelocity = 1;
                            }
                        }
                    }
                }
            }

            if(pMoving == true){
                switch(pDirection){
                    case 0:
                        pLocationX -= pVelocity;
                        if(curPlayerSprite < 3 && curPlayerSprite >= 0){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 0;
                        }
                        break;
                    case 1:
                        pLocationX += pVelocity;
                        if(curPlayerSprite < 7 && curPlayerSprite > 3){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 4;
                        }
                        break;
                    case 2:
                        pLocationY -= pVelocity;
                        if(curPlayerSprite < 11 && curPlayerSprite > 7){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 8;
                        }
                        break;
                    case 3:
                        pLocationY += pVelocity;
                        if(curPlayerSprite < 15 && curPlayerSprite > 11){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 12;
                        }
                        break;

                    case 4:
                        pLocationY -= pVelocity;
                        pLocationX -= pVelocity;
                        if(curPlayerSprite < 19 && curPlayerSprite > 15){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 16;
                        }
                        break;

                    case 5:
                        pLocationY -= pVelocity;
                        pLocationX += pVelocity;
                        if(curPlayerSprite < 23 && curPlayerSprite > 19){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 20;
                        }
                        break;

                    case 6:
                        pLocationY += pVelocity;
                        pLocationX -= pVelocity;
                        if(curPlayerSprite < 27 && curPlayerSprite > 23){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 24;
                        }
                        break;

                    case 7:
                        pLocationY += pVelocity;
                        pLocationX += pVelocity;
                        if(curPlayerSprite < 31 && curPlayerSprite > 27){
                            curPlayerSprite++;
                        } else {
                            curPlayerSprite = 28;
                        }
                        break;


                }

                camX = (pLocationX - Main.gWidth/2)  + playerWidth / 2;
                camY = (pLocationY - Main.gHeight/2)  + playerHeight / 2;

                pEnergy -= 1;


            } else {
                if(pEnergy <= 10000) {
                    pEnergy += 2;
                }
            }


            if(pShooting) {
                pAmmo -= 1;
                switch (pDirection) {
                    case 0:
                        endX = (pLocationX + playerWidth / 2) - 128;
                        endY = (pLocationY + playerHeight / 2);
                        break;
                    case 1:
                        endX = (pLocationX + playerWidth / 2) + 128;
                        endY = (pLocationY + playerHeight / 2);
                        break;
                    case 2:
                        endX = (pLocationX + playerWidth / 2);
                        endY = (pLocationY + playerHeight / 2) - 128;
                        break;
                    case 3:
                        endX = (pLocationX + playerWidth / 2);
                        endY = (pLocationY + playerHeight / 2) + 128;
                        break;
                    case 4:
                        endX = (pLocationX + playerWidth / 2) - 96;
                        endY = (pLocationY + playerHeight / 2) - 96;
                        break;
                    case 5:
                        endX = (pLocationX + playerWidth / 2) + 96;
                        endY = (pLocationY + playerHeight / 2) - 96;
                        break;
                    case 6:
                        endX = (pLocationX + playerWidth / 2) - 96;
                        endY = (pLocationY + playerHeight / 2) + 96;
                        break;
                    case 7:
                        endX = (pLocationX + playerWidth / 2) + 96;
                        endY = (pLocationY + playerHeight / 2) + 96;
                        break;

                }
                startX = (pLocationX + playerWidth / 2);
                startY = (pLocationY + playerHeight / 2);
                pEnergy -= 5;
            }
        }
    }
}