import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Bernard on 29-Oct-14.
 */
public class Menu extends JPanel implements ActionListener{

    public Menu(Manager managerclass){

        JButton start = new JButton();
        JButton options = new JButton();
        JButton quit = new JButton();

        start.setText("Start");
        options.setText("Options");
        quit.setText("Quit");

        start.addActionListener(this);
        options.addActionListener(this);
        quit.addActionListener(this);

        add(start);
        add(options);
        add(quit);

    }


    public void actionPerformed(ActionEvent e){
        if(e.getActionCommand() == "Start"){
           System.out.println("Start");
            Manager.gRunning = true;
        }

        if(e.getActionCommand() == "Options"){
            System.out.println(Manager.gRunning);
        }

        if(e.getActionCommand() == "Quit"){
            System.out.println("Quit");
            int reply = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit?", "Quit?",  JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION)
            {
                System.exit(0);
            }
        }
    }
}