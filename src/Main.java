import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Bernard on 29-Oct-14.
 */
public class Main extends JFrame{

    public static int gWidth = 800;
    public static int gHeight = 600;
    public static String gName = "Mech Game";

    public static void main(String[] args){

        Manager managerClass = new Manager();
        Menu panel = new Menu(managerClass);
        Game game = new Game(managerClass);

        JFrame frame = new JFrame();
        frame.setSize(gWidth, gHeight);
        frame.setTitle(gName);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(frame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                int reply = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit?", "Quit?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION)
                {
                    System.exit(0);
                }
            }
        });
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.addKeyListener(game);
        frame.setFocusable(true);
        while(true)
        {
            try {
                Thread.sleep(1000/30);
            } catch (InterruptedException ignore) {
            }

            if(managerClass.gRunning)
            {
                game.Update();
                frame.getContentPane().removeAll();
                frame.setContentPane(game);
                frame.revalidate();
                frame.repaint();
            }
        }
    }
}